﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Net;
using vkInvestigator.Lib.Extentions;

namespace vkInvestigator.Lib.Services
{
    public class Investigator
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;

        public Investigator(ILogger<Investigator> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }
        public string GetUserOnlineInfo(int userId)
        {
            try
            {
                // userId = 45237879; 104717760 - Лида
                var url = _configuration["VKAuthentication:user_info_uri"] +
                    $"?user_ids={userId}&fields=online,last_seen" +
                    _configuration["VKAuthentication:access_token_parameter"] +
                    _configuration["VKAuthentication:version_parameter"];
                var request = WebRequest.Create(url);
                string text;
                var response = (HttpWebResponse)request.GetResponse();

                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    text = sr.ReadToEnd();
                }
                return text;
            }
            catch(Exception ex)
            {
                _logger.Error(ex);
                return $"ERROR: {ex.Message}";
            }
        }
    }
}
