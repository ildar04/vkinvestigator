﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Hangfire;
using Abp.Modules;
using Abp.BackgroundJobs;
using Abp.Hangfire.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using vkInvestigator.Lib.Services;

namespace vkInvestigator.Lib.BackgroundServices
{
    [DependsOn(typeof(AbpHangfireAspNetCoreModule))]
    public class MyProjectWebModule : AbpModule
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;

        public MyProjectWebModule(ILogger<Investigator> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public override void PreInitialize()
        {
            Configuration.BackgroundJobs.UseHangfire();
        }

        //...
    }
}
