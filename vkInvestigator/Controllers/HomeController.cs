﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using vkInvestigator.Models;
using vkInvestigator.Lib.Extentions;
using vkInvestigator.Lib.Services;

namespace vkInvestigator.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Investigator _investigator;

        public HomeController(ILogger<HomeController> logger, Investigator investigator)
        {
            _logger = logger;
            _investigator = investigator;
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public IActionResult Index([FromBody]InvestigatorModel model)
        {
            try
            {
                var result = _investigator.GetUserOnlineInfo(model.userId);

                return Content(result);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Content($"ERROR: {ex.Message}");
            }
        }

        public IActionResult Test()
        {
            try
            {

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Content($"ERROR: {ex.Message}");
            }
            return View();
        }

        public IActionResult Webpack()
        {
            try
            {

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Content($"ERROR: {ex.Message}");
            }
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
