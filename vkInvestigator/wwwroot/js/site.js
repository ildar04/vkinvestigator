﻿new Vue({
    el: "#men",

    data: {
        dateNow: "",
        userId: "45237879",
        errorMessage: "",
        IsHasResponse: false,
        Id: "",
        first_name: "",
        last_name: "",
        online: "",
        last_seen_time: "",
        last_seen_platform: ""

    },

    methods: {
        submit: function () {

            this.dateNow = this.getDateNowString();

            if (this.userId === "") {

                this.response = "Please, set user Id";
                event.preventDefault();
            } else {
                axios.post('Home/Index', { userId: this.userId })
                    .then((response) => {

                        this.IsHasResponse = true;

                        var respText = response.data.response[0];

                        var onlineName = this.getOnlineFriendly(respText.online);
                        var date = this.toDateTimeString(respText.last_seen.time);
                        var platform = respText.last_seen.platform;
                        var platformName = this.getPlatformName(platform);

                        this.Id = respText.id;
                        this.first_name = respText.first_name;
                        this.last_name = respText.last_name;
                        this.online = onlineName;
                        this.last_seen_time = date;
                        this.last_seen_platform = platform + " - " + platformName;
                    })
                    .catch(function (error) {
                        this.errorMessage = 'ERROR: ' + error;
                        this.IsHasResponse = false;
                    });
            }
        },
        toDateTime: function (secs) {
            var t = new Date(1970, 0, 1);
            t.setSeconds(secs);
            return t;
        },
        toDateTimeString: function (dateSeconds) {
            var date = this.toDateTime(dateSeconds);
            var momentDate = moment(date);
            var timezoneOffset = -(new Date()).getTimezoneOffset();
            momentDate.add(timezoneOffset, 'minutes');
            var momentString = momentDate.format("DD.MM.YYYY h:mm:ss");
            return momentString;
        },
        getPlatformName: function (platformId) {
            var platformName = "Неизвестная платформа";
            switch (platformId) {
                case 1:
                    platformName = "Мобильная версия"; break;
                case 2:
                    platformName = "Приложение для iPhone"; break;
                case 3:
                    platformName = "Приложение для iPad"; break;
                case 4:
                    platformName = "Приложение для Android"; break;
                case 5:
                    platformName = "Приложение для Windows Phone"; break;
                case 6:
                    platformName = "Приложение для Windows 10"; break;
                case 7:
                    platformName = "Полная версия сайта"; break;
            }
            return platformName;
        },
        getOnlineFriendly: function (online) {
            var platformName = "Unknown status";
            switch (online) {
                case 0:
                    platformName = "offline"; break;
                case 1:
                    platformName = "online"; break;
            }
            return platformName;
        },
        getDateNowString: function () {
            var momentDate = moment();
            var momentString = momentDate.format("DD.MM.YYYY h:mm:ss");
            return momentString;
        }
    }
});