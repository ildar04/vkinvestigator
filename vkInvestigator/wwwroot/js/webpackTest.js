﻿var myModule1 = require('myModule');

var myModuleInstance = new myModule1();
myModuleInstance.hello(); // 'hello!'
myModuleInstance.goodbye(); // 'goodbye!'