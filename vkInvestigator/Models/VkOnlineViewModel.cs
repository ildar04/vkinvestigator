﻿namespace vkInvestigator.Models
{
    public class VkOnlineViewModel
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public int last_name { get; set; }
        public string online { get; set; }
        public last_seen last_seen { get; set; }
    }

    public class last_seen
    {
        public int time { get; set; }
        public int platform { get; set; }
    }
}
