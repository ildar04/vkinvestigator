FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 80

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY ["vkInvestigator/vkInvestigator.csproj", "vkInvestigator/"]
COPY ["vkInvestigator.Lib/vkInvestigator.Lib.csproj", "vkInvestigator.Lib/"]
RUN dotnet restore "vkInvestigator/vkInvestigator.csproj"
COPY . .
WORKDIR "/src/vkInvestigator"
RUN dotnet build "vkInvestigator.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "vkInvestigator.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "vkInvestigator.dll"]